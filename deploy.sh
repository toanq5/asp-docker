#!/bin/bash -v
set -e

echo "[systemd] Copy over units to 171.241.162.19."
rsync -crlDz $CI_PROJECT_DIR/systemd/ 171.241.162.19:~/myapp-infrastructure-systemd/

echo "[systemd] Sync configurations with /etc/systemd/ folder."
RESULT="$(ssh 171.241.162.19 "sudo rsync -crlDi ~/myapp-infrastructure-systemd/ /etc/systemd/")"

if [ -n "$RESULT" ]; then
        echo "[systemd] Updated files:"
        echo "$RESULT"
        echo "[systemd] Reloading systemd."
        ssh 171.241.162.19 "sudo systemctl daemon-reload"
else
        echo "[systemd] Nothing has changed."
fi